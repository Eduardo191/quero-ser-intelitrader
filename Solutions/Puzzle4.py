# coding: UTF-8
#Número mínimo de movimentos para resolução de uma torre de Hanoi
#Link: http://dojopuzzles.com/problemas/exibe/torres-de-hanoi/

def hanoiTower(height):
    if (height == 0):
        print('No movement required')
    elif(height == 1):
        print('Only one movement required')
    else:
        movements = (2 ** height) - 1
        print('{} movements required for {} discs'.format(movements, height))

hanoiTower(3)
hanoiTower(4)
hanoiTower(7)
hanoiTower(15)
hanoiTower(64)
hanoiTower(0)
hanoiTower(1)


