# coding: UTF-8
# Função que determina se ano é bissexto ou não
# Link: http://dojopuzzles.com/problemas/exibe/ano-bissexto/


def isLeapYear(year):
    if ((year % 4) and (year % 100)) == 0:
        if (year % 100) == 0:  
            if (year % 400) == 0:  
                print('{} is a leap year'.format(year))  
            else:  
                print('{} is not a leap year'.format(year))  
        else:  
            print('{} is a leap year'.format(year))  
    else:  
        print('{} is not a leap year'.format(year))  

isLeapYear(2010)
isLeapYear(2008)
isLeapYear(1888)
isLeapYear(2013)
isLeapYear(1889)
isLeapYear(1732)
