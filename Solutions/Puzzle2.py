#coding: UTF-8 
#Coversão de números romanos em indo-arábicos e vice versa
#Link: http://dojopuzzles.com/problemas/exibe/numeros-romanos/

def romanToInt(roman):
    if not roman:
        return 0

    dic = {"I":1, "V":5, "X":10, "L":50, "C":100, "D":500, "M":1000}
    n = len(roman)
    total = dic[roman[n-1]]

    for i in range(n-1,0,-1):
        current = dic[roman[i]]
        prev = dic[roman[i-1]]
        if (prev >= current):
            total += prev
        else:
            total -= prev
    print(total)

def intToRoman(integer):
    values = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1]
    symbols = ["M", "CM", "D", "CD","C", "XC", "L", "XL","X", "IX", "V", "IV","I"]
    romanNum = ''
    i = 0
    while (integer > 0):
        for _ in range(integer // values[i]):
            romanNum += symbols[i]
            integer -= values[i]
        i += 1
    print (romanNum)


romanToInt('IX')

intToRoman(2019)

